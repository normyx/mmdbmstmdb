package com.mgoulene.mmdb.cucumber;

import com.mgoulene.mmdb.MmdbmstmdbApp;
import io.cucumber.spring.CucumberContextConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.web.WebAppConfiguration;

@CucumberContextConfiguration
@SpringBootTest(classes = MmdbmstmdbApp.class)
@WebAppConfiguration
public class CucumberTestContextConfiguration {}
