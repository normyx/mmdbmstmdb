package com.mgoulene.mmdb.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.mgoulene.mmdb.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ImageDataDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ImageDataDTO.class);
        ImageDataDTO imageDataDTO1 = new ImageDataDTO();
        imageDataDTO1.setId("id1");
        ImageDataDTO imageDataDTO2 = new ImageDataDTO();
        assertThat(imageDataDTO1).isNotEqualTo(imageDataDTO2);
        imageDataDTO2.setId(imageDataDTO1.getId());
        assertThat(imageDataDTO1).isEqualTo(imageDataDTO2);
        imageDataDTO2.setId("id2");
        assertThat(imageDataDTO1).isNotEqualTo(imageDataDTO2);
        imageDataDTO1.setId(null);
        assertThat(imageDataDTO1).isNotEqualTo(imageDataDTO2);
    }
}
