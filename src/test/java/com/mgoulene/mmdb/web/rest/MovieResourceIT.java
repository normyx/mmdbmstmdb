package com.mgoulene.mmdb.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.mgoulene.mmdb.IntegrationTest;
import com.mgoulene.mmdb.domain.Movie;
import com.mgoulene.mmdb.domain.enumeration.MovieStatus;
import com.mgoulene.mmdb.repository.MovieRepository;
import com.mgoulene.mmdb.service.MovieService;
import com.mgoulene.mmdb.service.dto.MovieDTO;
import com.mgoulene.mmdb.service.mapper.MovieMapper;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

/**
 * Integration tests for the {@link MovieResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class MovieResourceIT {

    private static final String DEFAULT_TITLE = "AAAAAAAAAA";
    private static final String UPDATED_TITLE = "BBBBBBBBBB";

    private static final Boolean DEFAULT_FOR_ADULT = false;
    private static final Boolean UPDATED_FOR_ADULT = true;

    private static final String DEFAULT_HOMEPAGE = "AAAAAAAAAA";
    private static final String UPDATED_HOMEPAGE = "BBBBBBBBBB";

    private static final String DEFAULT_ORIGINAL_LANGUAGE = "AAAAAAAAAA";
    private static final String UPDATED_ORIGINAL_LANGUAGE = "BBBBBBBBBB";

    private static final String DEFAULT_ORIGINAL_TITLE = "AAAAAAAAAA";
    private static final String UPDATED_ORIGINAL_TITLE = "BBBBBBBBBB";

    private static final String DEFAULT_OVERVIEW = "AAAAAAAAAA";
    private static final String UPDATED_OVERVIEW = "BBBBBBBBBB";

    private static final String DEFAULT_TAGLINE = "AAAAAAAAAA";
    private static final String UPDATED_TAGLINE = "BBBBBBBBBB";

    private static final MovieStatus DEFAULT_STATUS = MovieStatus.RUMORED;
    private static final MovieStatus UPDATED_STATUS = MovieStatus.PLANNED;

    private static final Float DEFAULT_VOTE_AVERAGE = 0F;
    private static final Float UPDATED_VOTE_AVERAGE = 1F;

    private static final Integer DEFAULT_VOTE_COUNT = 1;
    private static final Integer UPDATED_VOTE_COUNT = 2;

    private static final LocalDate DEFAULT_RELEASE_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_RELEASE_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_LAST_TMDB_UPDATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LAST_TMDB_UPDATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_TMDB_ID = "AAAAAAAAAA";
    private static final String UPDATED_TMDB_ID = "BBBBBBBBBB";

    private static final Integer DEFAULT_RUNTIME = 1;
    private static final Integer UPDATED_RUNTIME = 2;

    private static final String ENTITY_API_URL = "/api/movies";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    @Autowired
    private MovieRepository movieRepository;

    @Mock
    private MovieRepository movieRepositoryMock;

    @Autowired
    private MovieMapper movieMapper;

    @Mock
    private MovieService movieServiceMock;

    @Autowired
    private MockMvc restMovieMockMvc;

    private Movie movie;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Movie createEntity() {
        Movie movie = new Movie()
            .title(DEFAULT_TITLE)
            .forAdult(DEFAULT_FOR_ADULT)
            .homepage(DEFAULT_HOMEPAGE)
            .originalLanguage(DEFAULT_ORIGINAL_LANGUAGE)
            .originalTitle(DEFAULT_ORIGINAL_TITLE)
            .overview(DEFAULT_OVERVIEW)
            .tagline(DEFAULT_TAGLINE)
            .status(DEFAULT_STATUS)
            .voteAverage(DEFAULT_VOTE_AVERAGE)
            .voteCount(DEFAULT_VOTE_COUNT)
            .releaseDate(DEFAULT_RELEASE_DATE)
            .lastTMDBUpdate(DEFAULT_LAST_TMDB_UPDATE)
            .tmdbId(DEFAULT_TMDB_ID)
            .runtime(DEFAULT_RUNTIME);
        return movie;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Movie createUpdatedEntity() {
        Movie movie = new Movie()
            .title(UPDATED_TITLE)
            .forAdult(UPDATED_FOR_ADULT)
            .homepage(UPDATED_HOMEPAGE)
            .originalLanguage(UPDATED_ORIGINAL_LANGUAGE)
            .originalTitle(UPDATED_ORIGINAL_TITLE)
            .overview(UPDATED_OVERVIEW)
            .tagline(UPDATED_TAGLINE)
            .status(UPDATED_STATUS)
            .voteAverage(UPDATED_VOTE_AVERAGE)
            .voteCount(UPDATED_VOTE_COUNT)
            .releaseDate(UPDATED_RELEASE_DATE)
            .lastTMDBUpdate(UPDATED_LAST_TMDB_UPDATE)
            .tmdbId(UPDATED_TMDB_ID)
            .runtime(UPDATED_RUNTIME);
        return movie;
    }

    @BeforeEach
    public void initTest() {
        movieRepository.deleteAll();
        movie = createEntity();
    }

    @Test
    void createMovie() throws Exception {
        int databaseSizeBeforeCreate = movieRepository.findAll().size();
        // Create the Movie
        MovieDTO movieDTO = movieMapper.toDto(movie);
        restMovieMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(movieDTO))
            )
            .andExpect(status().isCreated());

        // Validate the Movie in the database
        List<Movie> movieList = movieRepository.findAll();
        assertThat(movieList).hasSize(databaseSizeBeforeCreate + 1);
        Movie testMovie = movieList.get(movieList.size() - 1);
        assertThat(testMovie.getTitle()).isEqualTo(DEFAULT_TITLE);
        assertThat(testMovie.getForAdult()).isEqualTo(DEFAULT_FOR_ADULT);
        assertThat(testMovie.getHomepage()).isEqualTo(DEFAULT_HOMEPAGE);
        assertThat(testMovie.getOriginalLanguage()).isEqualTo(DEFAULT_ORIGINAL_LANGUAGE);
        assertThat(testMovie.getOriginalTitle()).isEqualTo(DEFAULT_ORIGINAL_TITLE);
        assertThat(testMovie.getOverview()).isEqualTo(DEFAULT_OVERVIEW);
        assertThat(testMovie.getTagline()).isEqualTo(DEFAULT_TAGLINE);
        assertThat(testMovie.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testMovie.getVoteAverage()).isEqualTo(DEFAULT_VOTE_AVERAGE);
        assertThat(testMovie.getVoteCount()).isEqualTo(DEFAULT_VOTE_COUNT);
        assertThat(testMovie.getReleaseDate()).isEqualTo(DEFAULT_RELEASE_DATE);
        assertThat(testMovie.getLastTMDBUpdate()).isEqualTo(DEFAULT_LAST_TMDB_UPDATE);
        assertThat(testMovie.getTmdbId()).isEqualTo(DEFAULT_TMDB_ID);
        assertThat(testMovie.getRuntime()).isEqualTo(DEFAULT_RUNTIME);
    }

    @Test
    void createMovieWithExistingId() throws Exception {
        // Create the Movie with an existing ID
        movie.setId("existing_id");
        MovieDTO movieDTO = movieMapper.toDto(movie);

        int databaseSizeBeforeCreate = movieRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restMovieMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(movieDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Movie in the database
        List<Movie> movieList = movieRepository.findAll();
        assertThat(movieList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    void checkLastTMDBUpdateIsRequired() throws Exception {
        int databaseSizeBeforeTest = movieRepository.findAll().size();
        // set the field null
        movie.setLastTMDBUpdate(null);

        // Create the Movie, which fails.
        MovieDTO movieDTO = movieMapper.toDto(movie);

        restMovieMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(movieDTO))
            )
            .andExpect(status().isBadRequest());

        List<Movie> movieList = movieRepository.findAll();
        assertThat(movieList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    void checkTmdbIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = movieRepository.findAll().size();
        // set the field null
        movie.setTmdbId(null);

        // Create the Movie, which fails.
        MovieDTO movieDTO = movieMapper.toDto(movie);

        restMovieMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(movieDTO))
            )
            .andExpect(status().isBadRequest());

        List<Movie> movieList = movieRepository.findAll();
        assertThat(movieList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    void getAllMovies() throws Exception {
        // Initialize the database
        movieRepository.save(movie);

        // Get all the movieList
        restMovieMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(movie.getId())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE)))
            .andExpect(jsonPath("$.[*].forAdult").value(hasItem(DEFAULT_FOR_ADULT.booleanValue())))
            .andExpect(jsonPath("$.[*].homepage").value(hasItem(DEFAULT_HOMEPAGE)))
            .andExpect(jsonPath("$.[*].originalLanguage").value(hasItem(DEFAULT_ORIGINAL_LANGUAGE)))
            .andExpect(jsonPath("$.[*].originalTitle").value(hasItem(DEFAULT_ORIGINAL_TITLE)))
            .andExpect(jsonPath("$.[*].overview").value(hasItem(DEFAULT_OVERVIEW)))
            .andExpect(jsonPath("$.[*].tagline").value(hasItem(DEFAULT_TAGLINE)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].voteAverage").value(hasItem(DEFAULT_VOTE_AVERAGE.doubleValue())))
            .andExpect(jsonPath("$.[*].voteCount").value(hasItem(DEFAULT_VOTE_COUNT)))
            .andExpect(jsonPath("$.[*].releaseDate").value(hasItem(DEFAULT_RELEASE_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastTMDBUpdate").value(hasItem(DEFAULT_LAST_TMDB_UPDATE.toString())))
            .andExpect(jsonPath("$.[*].tmdbId").value(hasItem(DEFAULT_TMDB_ID)))
            .andExpect(jsonPath("$.[*].runtime").value(hasItem(DEFAULT_RUNTIME)));
    }

    @SuppressWarnings({ "unchecked" })
    void getAllMoviesWithEagerRelationshipsIsEnabled() throws Exception {
        when(movieServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restMovieMockMvc.perform(get(ENTITY_API_URL + "?eagerload=true")).andExpect(status().isOk());

        verify(movieServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({ "unchecked" })
    void getAllMoviesWithEagerRelationshipsIsNotEnabled() throws Exception {
        when(movieServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restMovieMockMvc.perform(get(ENTITY_API_URL + "?eagerload=true")).andExpect(status().isOk());

        verify(movieServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    void getMovie() throws Exception {
        // Initialize the database
        movieRepository.save(movie);

        // Get the movie
        restMovieMockMvc
            .perform(get(ENTITY_API_URL_ID, movie.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(movie.getId()))
            .andExpect(jsonPath("$.title").value(DEFAULT_TITLE))
            .andExpect(jsonPath("$.forAdult").value(DEFAULT_FOR_ADULT.booleanValue()))
            .andExpect(jsonPath("$.homepage").value(DEFAULT_HOMEPAGE))
            .andExpect(jsonPath("$.originalLanguage").value(DEFAULT_ORIGINAL_LANGUAGE))
            .andExpect(jsonPath("$.originalTitle").value(DEFAULT_ORIGINAL_TITLE))
            .andExpect(jsonPath("$.overview").value(DEFAULT_OVERVIEW))
            .andExpect(jsonPath("$.tagline").value(DEFAULT_TAGLINE))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.voteAverage").value(DEFAULT_VOTE_AVERAGE.doubleValue()))
            .andExpect(jsonPath("$.voteCount").value(DEFAULT_VOTE_COUNT))
            .andExpect(jsonPath("$.releaseDate").value(DEFAULT_RELEASE_DATE.toString()))
            .andExpect(jsonPath("$.lastTMDBUpdate").value(DEFAULT_LAST_TMDB_UPDATE.toString()))
            .andExpect(jsonPath("$.tmdbId").value(DEFAULT_TMDB_ID))
            .andExpect(jsonPath("$.runtime").value(DEFAULT_RUNTIME));
    }

    @Test
    void getNonExistingMovie() throws Exception {
        // Get the movie
        restMovieMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    void putNewMovie() throws Exception {
        // Initialize the database
        movieRepository.save(movie);

        int databaseSizeBeforeUpdate = movieRepository.findAll().size();

        // Update the movie
        Movie updatedMovie = movieRepository.findById(movie.getId()).get();
        updatedMovie
            .title(UPDATED_TITLE)
            .forAdult(UPDATED_FOR_ADULT)
            .homepage(UPDATED_HOMEPAGE)
            .originalLanguage(UPDATED_ORIGINAL_LANGUAGE)
            .originalTitle(UPDATED_ORIGINAL_TITLE)
            .overview(UPDATED_OVERVIEW)
            .tagline(UPDATED_TAGLINE)
            .status(UPDATED_STATUS)
            .voteAverage(UPDATED_VOTE_AVERAGE)
            .voteCount(UPDATED_VOTE_COUNT)
            .releaseDate(UPDATED_RELEASE_DATE)
            .lastTMDBUpdate(UPDATED_LAST_TMDB_UPDATE)
            .tmdbId(UPDATED_TMDB_ID)
            .runtime(UPDATED_RUNTIME);
        MovieDTO movieDTO = movieMapper.toDto(updatedMovie);

        restMovieMockMvc
            .perform(
                put(ENTITY_API_URL_ID, movieDTO.getId())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(movieDTO))
            )
            .andExpect(status().isOk());

        // Validate the Movie in the database
        List<Movie> movieList = movieRepository.findAll();
        assertThat(movieList).hasSize(databaseSizeBeforeUpdate);
        Movie testMovie = movieList.get(movieList.size() - 1);
        assertThat(testMovie.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testMovie.getForAdult()).isEqualTo(UPDATED_FOR_ADULT);
        assertThat(testMovie.getHomepage()).isEqualTo(UPDATED_HOMEPAGE);
        assertThat(testMovie.getOriginalLanguage()).isEqualTo(UPDATED_ORIGINAL_LANGUAGE);
        assertThat(testMovie.getOriginalTitle()).isEqualTo(UPDATED_ORIGINAL_TITLE);
        assertThat(testMovie.getOverview()).isEqualTo(UPDATED_OVERVIEW);
        assertThat(testMovie.getTagline()).isEqualTo(UPDATED_TAGLINE);
        assertThat(testMovie.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testMovie.getVoteAverage()).isEqualTo(UPDATED_VOTE_AVERAGE);
        assertThat(testMovie.getVoteCount()).isEqualTo(UPDATED_VOTE_COUNT);
        assertThat(testMovie.getReleaseDate()).isEqualTo(UPDATED_RELEASE_DATE);
        assertThat(testMovie.getLastTMDBUpdate()).isEqualTo(UPDATED_LAST_TMDB_UPDATE);
        assertThat(testMovie.getTmdbId()).isEqualTo(UPDATED_TMDB_ID);
        assertThat(testMovie.getRuntime()).isEqualTo(UPDATED_RUNTIME);
    }

    @Test
    void putNonExistingMovie() throws Exception {
        int databaseSizeBeforeUpdate = movieRepository.findAll().size();
        movie.setId(UUID.randomUUID().toString());

        // Create the Movie
        MovieDTO movieDTO = movieMapper.toDto(movie);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMovieMockMvc
            .perform(
                put(ENTITY_API_URL_ID, movieDTO.getId())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(movieDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Movie in the database
        List<Movie> movieList = movieRepository.findAll();
        assertThat(movieList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithIdMismatchMovie() throws Exception {
        int databaseSizeBeforeUpdate = movieRepository.findAll().size();
        movie.setId(UUID.randomUUID().toString());

        // Create the Movie
        MovieDTO movieDTO = movieMapper.toDto(movie);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMovieMockMvc
            .perform(
                put(ENTITY_API_URL_ID, UUID.randomUUID().toString())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(movieDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Movie in the database
        List<Movie> movieList = movieRepository.findAll();
        assertThat(movieList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithMissingIdPathParamMovie() throws Exception {
        int databaseSizeBeforeUpdate = movieRepository.findAll().size();
        movie.setId(UUID.randomUUID().toString());

        // Create the Movie
        MovieDTO movieDTO = movieMapper.toDto(movie);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMovieMockMvc
            .perform(
                put(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(movieDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Movie in the database
        List<Movie> movieList = movieRepository.findAll();
        assertThat(movieList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void partialUpdateMovieWithPatch() throws Exception {
        // Initialize the database
        movieRepository.save(movie);

        int databaseSizeBeforeUpdate = movieRepository.findAll().size();

        // Update the movie using partial update
        Movie partialUpdatedMovie = new Movie();
        partialUpdatedMovie.setId(movie.getId());

        partialUpdatedMovie
            .title(UPDATED_TITLE)
            .forAdult(UPDATED_FOR_ADULT)
            .originalLanguage(UPDATED_ORIGINAL_LANGUAGE)
            .originalTitle(UPDATED_ORIGINAL_TITLE)
            .overview(UPDATED_OVERVIEW)
            .tagline(UPDATED_TAGLINE)
            .status(UPDATED_STATUS)
            .voteAverage(UPDATED_VOTE_AVERAGE)
            .voteCount(UPDATED_VOTE_COUNT)
            .releaseDate(UPDATED_RELEASE_DATE)
            .lastTMDBUpdate(UPDATED_LAST_TMDB_UPDATE);

        restMovieMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedMovie.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedMovie))
            )
            .andExpect(status().isOk());

        // Validate the Movie in the database
        List<Movie> movieList = movieRepository.findAll();
        assertThat(movieList).hasSize(databaseSizeBeforeUpdate);
        Movie testMovie = movieList.get(movieList.size() - 1);
        assertThat(testMovie.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testMovie.getForAdult()).isEqualTo(UPDATED_FOR_ADULT);
        assertThat(testMovie.getHomepage()).isEqualTo(DEFAULT_HOMEPAGE);
        assertThat(testMovie.getOriginalLanguage()).isEqualTo(UPDATED_ORIGINAL_LANGUAGE);
        assertThat(testMovie.getOriginalTitle()).isEqualTo(UPDATED_ORIGINAL_TITLE);
        assertThat(testMovie.getOverview()).isEqualTo(UPDATED_OVERVIEW);
        assertThat(testMovie.getTagline()).isEqualTo(UPDATED_TAGLINE);
        assertThat(testMovie.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testMovie.getVoteAverage()).isEqualTo(UPDATED_VOTE_AVERAGE);
        assertThat(testMovie.getVoteCount()).isEqualTo(UPDATED_VOTE_COUNT);
        assertThat(testMovie.getReleaseDate()).isEqualTo(UPDATED_RELEASE_DATE);
        assertThat(testMovie.getLastTMDBUpdate()).isEqualTo(UPDATED_LAST_TMDB_UPDATE);
        assertThat(testMovie.getTmdbId()).isEqualTo(DEFAULT_TMDB_ID);
        assertThat(testMovie.getRuntime()).isEqualTo(DEFAULT_RUNTIME);
    }

    @Test
    void fullUpdateMovieWithPatch() throws Exception {
        // Initialize the database
        movieRepository.save(movie);

        int databaseSizeBeforeUpdate = movieRepository.findAll().size();

        // Update the movie using partial update
        Movie partialUpdatedMovie = new Movie();
        partialUpdatedMovie.setId(movie.getId());

        partialUpdatedMovie
            .title(UPDATED_TITLE)
            .forAdult(UPDATED_FOR_ADULT)
            .homepage(UPDATED_HOMEPAGE)
            .originalLanguage(UPDATED_ORIGINAL_LANGUAGE)
            .originalTitle(UPDATED_ORIGINAL_TITLE)
            .overview(UPDATED_OVERVIEW)
            .tagline(UPDATED_TAGLINE)
            .status(UPDATED_STATUS)
            .voteAverage(UPDATED_VOTE_AVERAGE)
            .voteCount(UPDATED_VOTE_COUNT)
            .releaseDate(UPDATED_RELEASE_DATE)
            .lastTMDBUpdate(UPDATED_LAST_TMDB_UPDATE)
            .tmdbId(UPDATED_TMDB_ID)
            .runtime(UPDATED_RUNTIME);

        restMovieMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedMovie.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedMovie))
            )
            .andExpect(status().isOk());

        // Validate the Movie in the database
        List<Movie> movieList = movieRepository.findAll();
        assertThat(movieList).hasSize(databaseSizeBeforeUpdate);
        Movie testMovie = movieList.get(movieList.size() - 1);
        assertThat(testMovie.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testMovie.getForAdult()).isEqualTo(UPDATED_FOR_ADULT);
        assertThat(testMovie.getHomepage()).isEqualTo(UPDATED_HOMEPAGE);
        assertThat(testMovie.getOriginalLanguage()).isEqualTo(UPDATED_ORIGINAL_LANGUAGE);
        assertThat(testMovie.getOriginalTitle()).isEqualTo(UPDATED_ORIGINAL_TITLE);
        assertThat(testMovie.getOverview()).isEqualTo(UPDATED_OVERVIEW);
        assertThat(testMovie.getTagline()).isEqualTo(UPDATED_TAGLINE);
        assertThat(testMovie.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testMovie.getVoteAverage()).isEqualTo(UPDATED_VOTE_AVERAGE);
        assertThat(testMovie.getVoteCount()).isEqualTo(UPDATED_VOTE_COUNT);
        assertThat(testMovie.getReleaseDate()).isEqualTo(UPDATED_RELEASE_DATE);
        assertThat(testMovie.getLastTMDBUpdate()).isEqualTo(UPDATED_LAST_TMDB_UPDATE);
        assertThat(testMovie.getTmdbId()).isEqualTo(UPDATED_TMDB_ID);
        assertThat(testMovie.getRuntime()).isEqualTo(UPDATED_RUNTIME);
    }

    @Test
    void patchNonExistingMovie() throws Exception {
        int databaseSizeBeforeUpdate = movieRepository.findAll().size();
        movie.setId(UUID.randomUUID().toString());

        // Create the Movie
        MovieDTO movieDTO = movieMapper.toDto(movie);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMovieMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, movieDTO.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(movieDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Movie in the database
        List<Movie> movieList = movieRepository.findAll();
        assertThat(movieList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithIdMismatchMovie() throws Exception {
        int databaseSizeBeforeUpdate = movieRepository.findAll().size();
        movie.setId(UUID.randomUUID().toString());

        // Create the Movie
        MovieDTO movieDTO = movieMapper.toDto(movie);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMovieMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, UUID.randomUUID().toString())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(movieDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Movie in the database
        List<Movie> movieList = movieRepository.findAll();
        assertThat(movieList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithMissingIdPathParamMovie() throws Exception {
        int databaseSizeBeforeUpdate = movieRepository.findAll().size();
        movie.setId(UUID.randomUUID().toString());

        // Create the Movie
        MovieDTO movieDTO = movieMapper.toDto(movie);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMovieMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(movieDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Movie in the database
        List<Movie> movieList = movieRepository.findAll();
        assertThat(movieList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void deleteMovie() throws Exception {
        // Initialize the database
        movieRepository.save(movie);

        int databaseSizeBeforeDelete = movieRepository.findAll().size();

        // Delete the movie
        restMovieMockMvc
            .perform(delete(ENTITY_API_URL_ID, movie.getId()).with(csrf()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Movie> movieList = movieRepository.findAll();
        assertThat(movieList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
