package com.mgoulene.mmdb.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.mgoulene.mmdb.IntegrationTest;
import com.mgoulene.mmdb.domain.Credit;
import com.mgoulene.mmdb.repository.CreditRepository;
import com.mgoulene.mmdb.service.dto.CreditDTO;
import com.mgoulene.mmdb.service.mapper.CreditMapper;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

/**
 * Integration tests for the {@link CreditResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class CreditResourceIT {

    private static final String DEFAULT_TMDB_ID = "AAAAAAAAAA";
    private static final String UPDATED_TMDB_ID = "BBBBBBBBBB";

    private static final String DEFAULT_CHARACTER = "AAAAAAAAAA";
    private static final String UPDATED_CHARACTER = "BBBBBBBBBB";

    private static final String DEFAULT_CREDIT_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_CREDIT_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_DEPARTMENT = "AAAAAAAAAA";
    private static final String UPDATED_DEPARTMENT = "BBBBBBBBBB";

    private static final String DEFAULT_JOB = "AAAAAAAAAA";
    private static final String UPDATED_JOB = "BBBBBBBBBB";

    private static final Integer DEFAULT_ORDER = 1;
    private static final Integer UPDATED_ORDER = 2;

    private static final LocalDate DEFAULT_LAST_TMDB_UPDATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LAST_TMDB_UPDATE = LocalDate.now(ZoneId.systemDefault());

    private static final String ENTITY_API_URL = "/api/credits";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    @Autowired
    private CreditRepository creditRepository;

    @Autowired
    private CreditMapper creditMapper;

    @Autowired
    private MockMvc restCreditMockMvc;

    private Credit credit;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Credit createEntity() {
        Credit credit = new Credit()
            .tmdbId(DEFAULT_TMDB_ID)
            .character(DEFAULT_CHARACTER)
            .creditType(DEFAULT_CREDIT_TYPE)
            .department(DEFAULT_DEPARTMENT)
            .job(DEFAULT_JOB)
            .order(DEFAULT_ORDER)
            .lastTMDBUpdate(DEFAULT_LAST_TMDB_UPDATE);
        return credit;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Credit createUpdatedEntity() {
        Credit credit = new Credit()
            .tmdbId(UPDATED_TMDB_ID)
            .character(UPDATED_CHARACTER)
            .creditType(UPDATED_CREDIT_TYPE)
            .department(UPDATED_DEPARTMENT)
            .job(UPDATED_JOB)
            .order(UPDATED_ORDER)
            .lastTMDBUpdate(UPDATED_LAST_TMDB_UPDATE);
        return credit;
    }

    @BeforeEach
    public void initTest() {
        creditRepository.deleteAll();
        credit = createEntity();
    }

    @Test
    void createCredit() throws Exception {
        int databaseSizeBeforeCreate = creditRepository.findAll().size();
        // Create the Credit
        CreditDTO creditDTO = creditMapper.toDto(credit);
        restCreditMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(creditDTO))
            )
            .andExpect(status().isCreated());

        // Validate the Credit in the database
        List<Credit> creditList = creditRepository.findAll();
        assertThat(creditList).hasSize(databaseSizeBeforeCreate + 1);
        Credit testCredit = creditList.get(creditList.size() - 1);
        assertThat(testCredit.getTmdbId()).isEqualTo(DEFAULT_TMDB_ID);
        assertThat(testCredit.getCharacter()).isEqualTo(DEFAULT_CHARACTER);
        assertThat(testCredit.getCreditType()).isEqualTo(DEFAULT_CREDIT_TYPE);
        assertThat(testCredit.getDepartment()).isEqualTo(DEFAULT_DEPARTMENT);
        assertThat(testCredit.getJob()).isEqualTo(DEFAULT_JOB);
        assertThat(testCredit.getOrder()).isEqualTo(DEFAULT_ORDER);
        assertThat(testCredit.getLastTMDBUpdate()).isEqualTo(DEFAULT_LAST_TMDB_UPDATE);
    }

    @Test
    void createCreditWithExistingId() throws Exception {
        // Create the Credit with an existing ID
        credit.setId("existing_id");
        CreditDTO creditDTO = creditMapper.toDto(credit);

        int databaseSizeBeforeCreate = creditRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restCreditMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(creditDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Credit in the database
        List<Credit> creditList = creditRepository.findAll();
        assertThat(creditList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    void checkTmdbIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = creditRepository.findAll().size();
        // set the field null
        credit.setTmdbId(null);

        // Create the Credit, which fails.
        CreditDTO creditDTO = creditMapper.toDto(credit);

        restCreditMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(creditDTO))
            )
            .andExpect(status().isBadRequest());

        List<Credit> creditList = creditRepository.findAll();
        assertThat(creditList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    void checkLastTMDBUpdateIsRequired() throws Exception {
        int databaseSizeBeforeTest = creditRepository.findAll().size();
        // set the field null
        credit.setLastTMDBUpdate(null);

        // Create the Credit, which fails.
        CreditDTO creditDTO = creditMapper.toDto(credit);

        restCreditMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(creditDTO))
            )
            .andExpect(status().isBadRequest());

        List<Credit> creditList = creditRepository.findAll();
        assertThat(creditList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    void getAllCredits() throws Exception {
        // Initialize the database
        creditRepository.save(credit);

        // Get all the creditList
        restCreditMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(credit.getId())))
            .andExpect(jsonPath("$.[*].tmdbId").value(hasItem(DEFAULT_TMDB_ID)))
            .andExpect(jsonPath("$.[*].character").value(hasItem(DEFAULT_CHARACTER)))
            .andExpect(jsonPath("$.[*].creditType").value(hasItem(DEFAULT_CREDIT_TYPE)))
            .andExpect(jsonPath("$.[*].department").value(hasItem(DEFAULT_DEPARTMENT)))
            .andExpect(jsonPath("$.[*].job").value(hasItem(DEFAULT_JOB)))
            .andExpect(jsonPath("$.[*].order").value(hasItem(DEFAULT_ORDER)))
            .andExpect(jsonPath("$.[*].lastTMDBUpdate").value(hasItem(DEFAULT_LAST_TMDB_UPDATE.toString())));
    }

    @Test
    void getCredit() throws Exception {
        // Initialize the database
        creditRepository.save(credit);

        // Get the credit
        restCreditMockMvc
            .perform(get(ENTITY_API_URL_ID, credit.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(credit.getId()))
            .andExpect(jsonPath("$.tmdbId").value(DEFAULT_TMDB_ID))
            .andExpect(jsonPath("$.character").value(DEFAULT_CHARACTER))
            .andExpect(jsonPath("$.creditType").value(DEFAULT_CREDIT_TYPE))
            .andExpect(jsonPath("$.department").value(DEFAULT_DEPARTMENT))
            .andExpect(jsonPath("$.job").value(DEFAULT_JOB))
            .andExpect(jsonPath("$.order").value(DEFAULT_ORDER))
            .andExpect(jsonPath("$.lastTMDBUpdate").value(DEFAULT_LAST_TMDB_UPDATE.toString()));
    }

    @Test
    void getNonExistingCredit() throws Exception {
        // Get the credit
        restCreditMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    void putNewCredit() throws Exception {
        // Initialize the database
        creditRepository.save(credit);

        int databaseSizeBeforeUpdate = creditRepository.findAll().size();

        // Update the credit
        Credit updatedCredit = creditRepository.findById(credit.getId()).get();
        updatedCredit
            .tmdbId(UPDATED_TMDB_ID)
            .character(UPDATED_CHARACTER)
            .creditType(UPDATED_CREDIT_TYPE)
            .department(UPDATED_DEPARTMENT)
            .job(UPDATED_JOB)
            .order(UPDATED_ORDER)
            .lastTMDBUpdate(UPDATED_LAST_TMDB_UPDATE);
        CreditDTO creditDTO = creditMapper.toDto(updatedCredit);

        restCreditMockMvc
            .perform(
                put(ENTITY_API_URL_ID, creditDTO.getId())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(creditDTO))
            )
            .andExpect(status().isOk());

        // Validate the Credit in the database
        List<Credit> creditList = creditRepository.findAll();
        assertThat(creditList).hasSize(databaseSizeBeforeUpdate);
        Credit testCredit = creditList.get(creditList.size() - 1);
        assertThat(testCredit.getTmdbId()).isEqualTo(UPDATED_TMDB_ID);
        assertThat(testCredit.getCharacter()).isEqualTo(UPDATED_CHARACTER);
        assertThat(testCredit.getCreditType()).isEqualTo(UPDATED_CREDIT_TYPE);
        assertThat(testCredit.getDepartment()).isEqualTo(UPDATED_DEPARTMENT);
        assertThat(testCredit.getJob()).isEqualTo(UPDATED_JOB);
        assertThat(testCredit.getOrder()).isEqualTo(UPDATED_ORDER);
        assertThat(testCredit.getLastTMDBUpdate()).isEqualTo(UPDATED_LAST_TMDB_UPDATE);
    }

    @Test
    void putNonExistingCredit() throws Exception {
        int databaseSizeBeforeUpdate = creditRepository.findAll().size();
        credit.setId(UUID.randomUUID().toString());

        // Create the Credit
        CreditDTO creditDTO = creditMapper.toDto(credit);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCreditMockMvc
            .perform(
                put(ENTITY_API_URL_ID, creditDTO.getId())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(creditDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Credit in the database
        List<Credit> creditList = creditRepository.findAll();
        assertThat(creditList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithIdMismatchCredit() throws Exception {
        int databaseSizeBeforeUpdate = creditRepository.findAll().size();
        credit.setId(UUID.randomUUID().toString());

        // Create the Credit
        CreditDTO creditDTO = creditMapper.toDto(credit);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCreditMockMvc
            .perform(
                put(ENTITY_API_URL_ID, UUID.randomUUID().toString())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(creditDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Credit in the database
        List<Credit> creditList = creditRepository.findAll();
        assertThat(creditList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithMissingIdPathParamCredit() throws Exception {
        int databaseSizeBeforeUpdate = creditRepository.findAll().size();
        credit.setId(UUID.randomUUID().toString());

        // Create the Credit
        CreditDTO creditDTO = creditMapper.toDto(credit);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCreditMockMvc
            .perform(
                put(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(creditDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Credit in the database
        List<Credit> creditList = creditRepository.findAll();
        assertThat(creditList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void partialUpdateCreditWithPatch() throws Exception {
        // Initialize the database
        creditRepository.save(credit);

        int databaseSizeBeforeUpdate = creditRepository.findAll().size();

        // Update the credit using partial update
        Credit partialUpdatedCredit = new Credit();
        partialUpdatedCredit.setId(credit.getId());

        partialUpdatedCredit.tmdbId(UPDATED_TMDB_ID).department(UPDATED_DEPARTMENT).order(UPDATED_ORDER);

        restCreditMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCredit.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCredit))
            )
            .andExpect(status().isOk());

        // Validate the Credit in the database
        List<Credit> creditList = creditRepository.findAll();
        assertThat(creditList).hasSize(databaseSizeBeforeUpdate);
        Credit testCredit = creditList.get(creditList.size() - 1);
        assertThat(testCredit.getTmdbId()).isEqualTo(UPDATED_TMDB_ID);
        assertThat(testCredit.getCharacter()).isEqualTo(DEFAULT_CHARACTER);
        assertThat(testCredit.getCreditType()).isEqualTo(DEFAULT_CREDIT_TYPE);
        assertThat(testCredit.getDepartment()).isEqualTo(UPDATED_DEPARTMENT);
        assertThat(testCredit.getJob()).isEqualTo(DEFAULT_JOB);
        assertThat(testCredit.getOrder()).isEqualTo(UPDATED_ORDER);
        assertThat(testCredit.getLastTMDBUpdate()).isEqualTo(DEFAULT_LAST_TMDB_UPDATE);
    }

    @Test
    void fullUpdateCreditWithPatch() throws Exception {
        // Initialize the database
        creditRepository.save(credit);

        int databaseSizeBeforeUpdate = creditRepository.findAll().size();

        // Update the credit using partial update
        Credit partialUpdatedCredit = new Credit();
        partialUpdatedCredit.setId(credit.getId());

        partialUpdatedCredit
            .tmdbId(UPDATED_TMDB_ID)
            .character(UPDATED_CHARACTER)
            .creditType(UPDATED_CREDIT_TYPE)
            .department(UPDATED_DEPARTMENT)
            .job(UPDATED_JOB)
            .order(UPDATED_ORDER)
            .lastTMDBUpdate(UPDATED_LAST_TMDB_UPDATE);

        restCreditMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCredit.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCredit))
            )
            .andExpect(status().isOk());

        // Validate the Credit in the database
        List<Credit> creditList = creditRepository.findAll();
        assertThat(creditList).hasSize(databaseSizeBeforeUpdate);
        Credit testCredit = creditList.get(creditList.size() - 1);
        assertThat(testCredit.getTmdbId()).isEqualTo(UPDATED_TMDB_ID);
        assertThat(testCredit.getCharacter()).isEqualTo(UPDATED_CHARACTER);
        assertThat(testCredit.getCreditType()).isEqualTo(UPDATED_CREDIT_TYPE);
        assertThat(testCredit.getDepartment()).isEqualTo(UPDATED_DEPARTMENT);
        assertThat(testCredit.getJob()).isEqualTo(UPDATED_JOB);
        assertThat(testCredit.getOrder()).isEqualTo(UPDATED_ORDER);
        assertThat(testCredit.getLastTMDBUpdate()).isEqualTo(UPDATED_LAST_TMDB_UPDATE);
    }

    @Test
    void patchNonExistingCredit() throws Exception {
        int databaseSizeBeforeUpdate = creditRepository.findAll().size();
        credit.setId(UUID.randomUUID().toString());

        // Create the Credit
        CreditDTO creditDTO = creditMapper.toDto(credit);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCreditMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, creditDTO.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(creditDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Credit in the database
        List<Credit> creditList = creditRepository.findAll();
        assertThat(creditList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithIdMismatchCredit() throws Exception {
        int databaseSizeBeforeUpdate = creditRepository.findAll().size();
        credit.setId(UUID.randomUUID().toString());

        // Create the Credit
        CreditDTO creditDTO = creditMapper.toDto(credit);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCreditMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, UUID.randomUUID().toString())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(creditDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Credit in the database
        List<Credit> creditList = creditRepository.findAll();
        assertThat(creditList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithMissingIdPathParamCredit() throws Exception {
        int databaseSizeBeforeUpdate = creditRepository.findAll().size();
        credit.setId(UUID.randomUUID().toString());

        // Create the Credit
        CreditDTO creditDTO = creditMapper.toDto(credit);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCreditMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(creditDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Credit in the database
        List<Credit> creditList = creditRepository.findAll();
        assertThat(creditList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void deleteCredit() throws Exception {
        // Initialize the database
        creditRepository.save(credit);

        int databaseSizeBeforeDelete = creditRepository.findAll().size();

        // Delete the credit
        restCreditMockMvc
            .perform(delete(ENTITY_API_URL_ID, credit.getId()).with(csrf()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Credit> creditList = creditRepository.findAll();
        assertThat(creditList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
