package com.mgoulene.mmdb.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.mgoulene.mmdb.IntegrationTest;
import com.mgoulene.mmdb.domain.Image;
import com.mgoulene.mmdb.domain.ImageData;
import com.mgoulene.mmdb.repository.ImageDataRepository;
import com.mgoulene.mmdb.service.dto.ImageDataDTO;
import com.mgoulene.mmdb.service.mapper.ImageDataMapper;
import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.Base64Utils;

/**
 * Integration tests for the {@link ImageDataResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class ImageDataResourceIT {

    private static final String DEFAULT_IMAGE_SIZE = "AAAAAAAAAA";
    private static final String UPDATED_IMAGE_SIZE = "BBBBBBBBBB";

    private static final byte[] DEFAULT_IMAGE_BYTES = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_IMAGE_BYTES = TestUtil.createByteArray(1, "1");
    private static final String DEFAULT_IMAGE_BYTES_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_IMAGE_BYTES_CONTENT_TYPE = "image/png";

    private static final String ENTITY_API_URL = "/api/image-data";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    @Autowired
    private ImageDataRepository imageDataRepository;

    @Autowired
    private ImageDataMapper imageDataMapper;

    @Autowired
    private MockMvc restImageDataMockMvc;

    private ImageData imageData;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ImageData createEntity() {
        ImageData imageData = new ImageData()
            .imageSize(DEFAULT_IMAGE_SIZE)
            .imageBytes(DEFAULT_IMAGE_BYTES)
            .imageBytesContentType(DEFAULT_IMAGE_BYTES_CONTENT_TYPE);
        // Add required entity
        Image image;
        image = ImageResourceIT.createEntity();
        image.setId("fixed-id-for-tests");
        imageData.setImage(image);
        return imageData;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ImageData createUpdatedEntity() {
        ImageData imageData = new ImageData()
            .imageSize(UPDATED_IMAGE_SIZE)
            .imageBytes(UPDATED_IMAGE_BYTES)
            .imageBytesContentType(UPDATED_IMAGE_BYTES_CONTENT_TYPE);
        // Add required entity
        Image image;
        image = ImageResourceIT.createUpdatedEntity();
        image.setId("fixed-id-for-tests");
        imageData.setImage(image);
        return imageData;
    }

    @BeforeEach
    public void initTest() {
        imageDataRepository.deleteAll();
        imageData = createEntity();
    }

    @Test
    void createImageData() throws Exception {
        int databaseSizeBeforeCreate = imageDataRepository.findAll().size();
        // Create the ImageData
        ImageDataDTO imageDataDTO = imageDataMapper.toDto(imageData);
        restImageDataMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(imageDataDTO))
            )
            .andExpect(status().isCreated());

        // Validate the ImageData in the database
        List<ImageData> imageDataList = imageDataRepository.findAll();
        assertThat(imageDataList).hasSize(databaseSizeBeforeCreate + 1);
        ImageData testImageData = imageDataList.get(imageDataList.size() - 1);
        assertThat(testImageData.getImageSize()).isEqualTo(DEFAULT_IMAGE_SIZE);
        assertThat(testImageData.getImageBytes()).isEqualTo(DEFAULT_IMAGE_BYTES);
        assertThat(testImageData.getImageBytesContentType()).isEqualTo(DEFAULT_IMAGE_BYTES_CONTENT_TYPE);
    }

    @Test
    void createImageDataWithExistingId() throws Exception {
        // Create the ImageData with an existing ID
        imageData.setId("existing_id");
        ImageDataDTO imageDataDTO = imageDataMapper.toDto(imageData);

        int databaseSizeBeforeCreate = imageDataRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restImageDataMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(imageDataDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ImageData in the database
        List<ImageData> imageDataList = imageDataRepository.findAll();
        assertThat(imageDataList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    void checkImageSizeIsRequired() throws Exception {
        int databaseSizeBeforeTest = imageDataRepository.findAll().size();
        // set the field null
        imageData.setImageSize(null);

        // Create the ImageData, which fails.
        ImageDataDTO imageDataDTO = imageDataMapper.toDto(imageData);

        restImageDataMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(imageDataDTO))
            )
            .andExpect(status().isBadRequest());

        List<ImageData> imageDataList = imageDataRepository.findAll();
        assertThat(imageDataList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    void getAllImageData() throws Exception {
        // Initialize the database
        imageDataRepository.save(imageData);

        // Get all the imageDataList
        restImageDataMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(imageData.getId())))
            .andExpect(jsonPath("$.[*].imageSize").value(hasItem(DEFAULT_IMAGE_SIZE)))
            .andExpect(jsonPath("$.[*].imageBytesContentType").value(hasItem(DEFAULT_IMAGE_BYTES_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].imageBytes").value(hasItem(Base64Utils.encodeToString(DEFAULT_IMAGE_BYTES))));
    }

    @Test
    void getImageData() throws Exception {
        // Initialize the database
        imageDataRepository.save(imageData);

        // Get the imageData
        restImageDataMockMvc
            .perform(get(ENTITY_API_URL_ID, imageData.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(imageData.getId()))
            .andExpect(jsonPath("$.imageSize").value(DEFAULT_IMAGE_SIZE))
            .andExpect(jsonPath("$.imageBytesContentType").value(DEFAULT_IMAGE_BYTES_CONTENT_TYPE))
            .andExpect(jsonPath("$.imageBytes").value(Base64Utils.encodeToString(DEFAULT_IMAGE_BYTES)));
    }

    @Test
    void getNonExistingImageData() throws Exception {
        // Get the imageData
        restImageDataMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    void putNewImageData() throws Exception {
        // Initialize the database
        imageDataRepository.save(imageData);

        int databaseSizeBeforeUpdate = imageDataRepository.findAll().size();

        // Update the imageData
        ImageData updatedImageData = imageDataRepository.findById(imageData.getId()).get();
        updatedImageData
            .imageSize(UPDATED_IMAGE_SIZE)
            .imageBytes(UPDATED_IMAGE_BYTES)
            .imageBytesContentType(UPDATED_IMAGE_BYTES_CONTENT_TYPE);
        ImageDataDTO imageDataDTO = imageDataMapper.toDto(updatedImageData);

        restImageDataMockMvc
            .perform(
                put(ENTITY_API_URL_ID, imageDataDTO.getId())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(imageDataDTO))
            )
            .andExpect(status().isOk());

        // Validate the ImageData in the database
        List<ImageData> imageDataList = imageDataRepository.findAll();
        assertThat(imageDataList).hasSize(databaseSizeBeforeUpdate);
        ImageData testImageData = imageDataList.get(imageDataList.size() - 1);
        assertThat(testImageData.getImageSize()).isEqualTo(UPDATED_IMAGE_SIZE);
        assertThat(testImageData.getImageBytes()).isEqualTo(UPDATED_IMAGE_BYTES);
        assertThat(testImageData.getImageBytesContentType()).isEqualTo(UPDATED_IMAGE_BYTES_CONTENT_TYPE);
    }

    @Test
    void putNonExistingImageData() throws Exception {
        int databaseSizeBeforeUpdate = imageDataRepository.findAll().size();
        imageData.setId(UUID.randomUUID().toString());

        // Create the ImageData
        ImageDataDTO imageDataDTO = imageDataMapper.toDto(imageData);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restImageDataMockMvc
            .perform(
                put(ENTITY_API_URL_ID, imageDataDTO.getId())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(imageDataDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ImageData in the database
        List<ImageData> imageDataList = imageDataRepository.findAll();
        assertThat(imageDataList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithIdMismatchImageData() throws Exception {
        int databaseSizeBeforeUpdate = imageDataRepository.findAll().size();
        imageData.setId(UUID.randomUUID().toString());

        // Create the ImageData
        ImageDataDTO imageDataDTO = imageDataMapper.toDto(imageData);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restImageDataMockMvc
            .perform(
                put(ENTITY_API_URL_ID, UUID.randomUUID().toString())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(imageDataDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ImageData in the database
        List<ImageData> imageDataList = imageDataRepository.findAll();
        assertThat(imageDataList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithMissingIdPathParamImageData() throws Exception {
        int databaseSizeBeforeUpdate = imageDataRepository.findAll().size();
        imageData.setId(UUID.randomUUID().toString());

        // Create the ImageData
        ImageDataDTO imageDataDTO = imageDataMapper.toDto(imageData);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restImageDataMockMvc
            .perform(
                put(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(imageDataDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the ImageData in the database
        List<ImageData> imageDataList = imageDataRepository.findAll();
        assertThat(imageDataList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void partialUpdateImageDataWithPatch() throws Exception {
        // Initialize the database
        imageDataRepository.save(imageData);

        int databaseSizeBeforeUpdate = imageDataRepository.findAll().size();

        // Update the imageData using partial update
        ImageData partialUpdatedImageData = new ImageData();
        partialUpdatedImageData.setId(imageData.getId());

        restImageDataMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedImageData.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedImageData))
            )
            .andExpect(status().isOk());

        // Validate the ImageData in the database
        List<ImageData> imageDataList = imageDataRepository.findAll();
        assertThat(imageDataList).hasSize(databaseSizeBeforeUpdate);
        ImageData testImageData = imageDataList.get(imageDataList.size() - 1);
        assertThat(testImageData.getImageSize()).isEqualTo(DEFAULT_IMAGE_SIZE);
        assertThat(testImageData.getImageBytes()).isEqualTo(DEFAULT_IMAGE_BYTES);
        assertThat(testImageData.getImageBytesContentType()).isEqualTo(DEFAULT_IMAGE_BYTES_CONTENT_TYPE);
    }

    @Test
    void fullUpdateImageDataWithPatch() throws Exception {
        // Initialize the database
        imageDataRepository.save(imageData);

        int databaseSizeBeforeUpdate = imageDataRepository.findAll().size();

        // Update the imageData using partial update
        ImageData partialUpdatedImageData = new ImageData();
        partialUpdatedImageData.setId(imageData.getId());

        partialUpdatedImageData
            .imageSize(UPDATED_IMAGE_SIZE)
            .imageBytes(UPDATED_IMAGE_BYTES)
            .imageBytesContentType(UPDATED_IMAGE_BYTES_CONTENT_TYPE);

        restImageDataMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedImageData.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedImageData))
            )
            .andExpect(status().isOk());

        // Validate the ImageData in the database
        List<ImageData> imageDataList = imageDataRepository.findAll();
        assertThat(imageDataList).hasSize(databaseSizeBeforeUpdate);
        ImageData testImageData = imageDataList.get(imageDataList.size() - 1);
        assertThat(testImageData.getImageSize()).isEqualTo(UPDATED_IMAGE_SIZE);
        assertThat(testImageData.getImageBytes()).isEqualTo(UPDATED_IMAGE_BYTES);
        assertThat(testImageData.getImageBytesContentType()).isEqualTo(UPDATED_IMAGE_BYTES_CONTENT_TYPE);
    }

    @Test
    void patchNonExistingImageData() throws Exception {
        int databaseSizeBeforeUpdate = imageDataRepository.findAll().size();
        imageData.setId(UUID.randomUUID().toString());

        // Create the ImageData
        ImageDataDTO imageDataDTO = imageDataMapper.toDto(imageData);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restImageDataMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, imageDataDTO.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(imageDataDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ImageData in the database
        List<ImageData> imageDataList = imageDataRepository.findAll();
        assertThat(imageDataList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithIdMismatchImageData() throws Exception {
        int databaseSizeBeforeUpdate = imageDataRepository.findAll().size();
        imageData.setId(UUID.randomUUID().toString());

        // Create the ImageData
        ImageDataDTO imageDataDTO = imageDataMapper.toDto(imageData);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restImageDataMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, UUID.randomUUID().toString())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(imageDataDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ImageData in the database
        List<ImageData> imageDataList = imageDataRepository.findAll();
        assertThat(imageDataList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithMissingIdPathParamImageData() throws Exception {
        int databaseSizeBeforeUpdate = imageDataRepository.findAll().size();
        imageData.setId(UUID.randomUUID().toString());

        // Create the ImageData
        ImageDataDTO imageDataDTO = imageDataMapper.toDto(imageData);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restImageDataMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(imageDataDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the ImageData in the database
        List<ImageData> imageDataList = imageDataRepository.findAll();
        assertThat(imageDataList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void deleteImageData() throws Exception {
        // Initialize the database
        imageDataRepository.save(imageData);

        int databaseSizeBeforeDelete = imageDataRepository.findAll().size();

        // Delete the imageData
        restImageDataMockMvc
            .perform(delete(ENTITY_API_URL_ID, imageData.getId()).with(csrf()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ImageData> imageDataList = imageDataRepository.findAll();
        assertThat(imageDataList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
