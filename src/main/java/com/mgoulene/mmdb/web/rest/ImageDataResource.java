package com.mgoulene.mmdb.web.rest;

import com.mgoulene.mmdb.repository.ImageDataRepository;
import com.mgoulene.mmdb.service.ImageDataService;
import com.mgoulene.mmdb.service.dto.ImageDataDTO;
import com.mgoulene.mmdb.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.mgoulene.mmdb.domain.ImageData}.
 */
@RestController
@RequestMapping("/api")
public class ImageDataResource {

    private final Logger log = LoggerFactory.getLogger(ImageDataResource.class);

    private static final String ENTITY_NAME = "mmdbmstmdbImageData";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ImageDataService imageDataService;

    private final ImageDataRepository imageDataRepository;

    public ImageDataResource(ImageDataService imageDataService, ImageDataRepository imageDataRepository) {
        this.imageDataService = imageDataService;
        this.imageDataRepository = imageDataRepository;
    }

    /**
     * {@code POST  /image-data} : Create a new imageData.
     *
     * @param imageDataDTO the imageDataDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new imageDataDTO, or with status {@code 400 (Bad Request)} if the imageData has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/image-data")
    public ResponseEntity<ImageDataDTO> createImageData(@Valid @RequestBody ImageDataDTO imageDataDTO) throws URISyntaxException {
        log.debug("REST request to save ImageData : {}", imageDataDTO);
        if (imageDataDTO.getId() != null) {
            throw new BadRequestAlertException("A new imageData cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ImageDataDTO result = imageDataService.save(imageDataDTO);
        return ResponseEntity
            .created(new URI("/api/image-data/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId()))
            .body(result);
    }

    /**
     * {@code PUT  /image-data/:id} : Updates an existing imageData.
     *
     * @param id the id of the imageDataDTO to save.
     * @param imageDataDTO the imageDataDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated imageDataDTO,
     * or with status {@code 400 (Bad Request)} if the imageDataDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the imageDataDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/image-data/{id}")
    public ResponseEntity<ImageDataDTO> updateImageData(
        @PathVariable(value = "id", required = false) final String id,
        @Valid @RequestBody ImageDataDTO imageDataDTO
    ) throws URISyntaxException {
        log.debug("REST request to update ImageData : {}, {}", id, imageDataDTO);
        if (imageDataDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, imageDataDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!imageDataRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        ImageDataDTO result = imageDataService.save(imageDataDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, imageDataDTO.getId()))
            .body(result);
    }

    /**
     * {@code PATCH  /image-data/:id} : Partial updates given fields of an existing imageData, field will ignore if it is null
     *
     * @param id the id of the imageDataDTO to save.
     * @param imageDataDTO the imageDataDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated imageDataDTO,
     * or with status {@code 400 (Bad Request)} if the imageDataDTO is not valid,
     * or with status {@code 404 (Not Found)} if the imageDataDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the imageDataDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/image-data/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<ImageDataDTO> partialUpdateImageData(
        @PathVariable(value = "id", required = false) final String id,
        @NotNull @RequestBody ImageDataDTO imageDataDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update ImageData partially : {}, {}", id, imageDataDTO);
        if (imageDataDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, imageDataDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!imageDataRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<ImageDataDTO> result = imageDataService.partialUpdate(imageDataDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, imageDataDTO.getId())
        );
    }

    /**
     * {@code GET  /image-data} : get all the imageData.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of imageData in body.
     */
    @GetMapping("/image-data")
    public ResponseEntity<List<ImageDataDTO>> getAllImageData(Pageable pageable) {
        log.debug("REST request to get a page of ImageData");
        Page<ImageDataDTO> page = imageDataService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /image-data/:id} : get the "id" imageData.
     *
     * @param id the id of the imageDataDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the imageDataDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/image-data/{id}")
    public ResponseEntity<ImageDataDTO> getImageData(@PathVariable String id) {
        log.debug("REST request to get ImageData : {}", id);
        Optional<ImageDataDTO> imageDataDTO = imageDataService.findOne(id);
        return ResponseUtil.wrapOrNotFound(imageDataDTO);
    }

    /**
     * {@code DELETE  /image-data/:id} : delete the "id" imageData.
     *
     * @param id the id of the imageDataDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/image-data/{id}")
    public ResponseEntity<Void> deleteImageData(@PathVariable String id) {
        log.debug("REST request to delete ImageData : {}", id);
        imageDataService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id)).build();
    }
}
