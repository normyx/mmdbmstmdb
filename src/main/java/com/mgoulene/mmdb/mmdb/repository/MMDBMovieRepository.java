package com.mgoulene.mmdb.mmdb.repository;

import com.mgoulene.mmdb.domain.Movie;
import com.mgoulene.mmdb.repository.MovieRepository;
import java.util.Optional;
import org.springframework.stereotype.Repository;

@Repository
public interface MMDBMovieRepository extends MovieRepository {
    Optional<Movie> findByTmdbId(String tmdbId);
}
