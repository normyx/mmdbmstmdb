package com.mgoulene.mmdb.mmdb.service;

import com.mgoulene.mmdb.domain.Movie;
import com.mgoulene.mmdb.mmdb.repository.MMDBMovieRepository;
import com.mgoulene.mmdb.service.MovieService;
import com.mgoulene.mmdb.service.dto.MovieDTO;
import com.mgoulene.mmdb.service.mapper.MovieMapper;
import com.mgoulene.mmdb.tmdb.repository.TMDBMovieRepository;
import com.mgoulene.mmdb.tmdb.service.mapper.TMDBMovieMapper;
import java.time.LocalDate;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * Service Implementation for managing {@link Movie}.
 */
@Service
public class MMDBMovieService extends MovieService {

    private final Logger log = LoggerFactory.getLogger(MMDBMovieService.class);

    private final MMDBMovieRepository mmdbMovieRepository;

    private final TMDBMovieRepository tmdbMovieRepository;

    private final TMDBMovieMapper tmdbMovieMapper;

    public MMDBMovieService(
        MMDBMovieRepository movieRepository,
        TMDBMovieRepository tmdbMovieRepository,
        MovieMapper movieMapper,
        TMDBMovieMapper tmdbMovieMapper
    ) {
        super(movieRepository, movieMapper);
        this.mmdbMovieRepository = movieRepository;
        this.tmdbMovieRepository = tmdbMovieRepository;
        this.tmdbMovieMapper = tmdbMovieMapper;
    }

    /**
     * Get one movie by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    public Optional<MovieDTO> findOneByTmdbId(String tmdbId) {
        log.debug("Request to get Movie from tmdbId : {}", tmdbId);
        Optional<MovieDTO> movieOpt = mmdbMovieRepository.findByTmdbId(tmdbId).map(movieMapper::toDto);
        if (movieOpt.isPresent()) {
            return movieOpt;
        } else {
            movieOpt = tmdbMovieRepository.findOneTMDBMovie(tmdbId).map(tmdbMovieMapper::toDTO);
            if (movieOpt.isPresent()) {
                Movie movie = movieMapper.toEntity(movieOpt.get());
                movie.setLastTMDBUpdate(LocalDate.now());
                return Optional.of(movieMapper.toDto(mmdbMovieRepository.save(movie)));
            } else {
                return Optional.empty();
            }
        }
        //return mmdbMovieRepository.findByTmdbId(tmdbId).map(movieMapper::toDto);
    }
}
