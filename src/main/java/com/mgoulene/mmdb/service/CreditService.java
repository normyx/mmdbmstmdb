package com.mgoulene.mmdb.service;

import com.mgoulene.mmdb.domain.Credit;
import com.mgoulene.mmdb.repository.CreditRepository;
import com.mgoulene.mmdb.service.dto.CreditDTO;
import com.mgoulene.mmdb.service.mapper.CreditMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * Service Implementation for managing {@link Credit}.
 */
@Service
public class CreditService {

    private final Logger log = LoggerFactory.getLogger(CreditService.class);

    private final CreditRepository creditRepository;

    private final CreditMapper creditMapper;

    public CreditService(CreditRepository creditRepository, CreditMapper creditMapper) {
        this.creditRepository = creditRepository;
        this.creditMapper = creditMapper;
    }

    /**
     * Save a credit.
     *
     * @param creditDTO the entity to save.
     * @return the persisted entity.
     */
    public CreditDTO save(CreditDTO creditDTO) {
        log.debug("Request to save Credit : {}", creditDTO);
        Credit credit = creditMapper.toEntity(creditDTO);
        credit = creditRepository.save(credit);
        return creditMapper.toDto(credit);
    }

    /**
     * Partially update a credit.
     *
     * @param creditDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<CreditDTO> partialUpdate(CreditDTO creditDTO) {
        log.debug("Request to partially update Credit : {}", creditDTO);

        return creditRepository
            .findById(creditDTO.getId())
            .map(
                existingCredit -> {
                    creditMapper.partialUpdate(existingCredit, creditDTO);
                    return existingCredit;
                }
            )
            .map(creditRepository::save)
            .map(creditMapper::toDto);
    }

    /**
     * Get all the credits.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    public Page<CreditDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Credits");
        return creditRepository.findAll(pageable).map(creditMapper::toDto);
    }

    /**
     * Get one credit by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    public Optional<CreditDTO> findOne(String id) {
        log.debug("Request to get Credit : {}", id);
        return creditRepository.findById(id).map(creditMapper::toDto);
    }

    /**
     * Delete the credit by id.
     *
     * @param id the id of the entity.
     */
    public void delete(String id) {
        log.debug("Request to delete Credit : {}", id);
        creditRepository.deleteById(id);
    }
}
