package com.mgoulene.mmdb.service.dto;

import io.swagger.annotations.ApiModel;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.mgoulene.mmdb.domain.Image} entity.
 */
@ApiModel(description = "The Image entity.\n@author A true hipster")
public class ImageDTO implements Serializable {

    private String id;

    @NotNull
    private String tmdbId;

    private String locale;

    private Float voteAverage;

    private Integer voteCount;

    @NotNull
    private LocalDate lastTMDBUpdate;

    private MovieDTO posterMovie;

    private MovieDTO backdropMovie;

    private PersonDTO person;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTmdbId() {
        return tmdbId;
    }

    public void setTmdbId(String tmdbId) {
        this.tmdbId = tmdbId;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public Float getVoteAverage() {
        return voteAverage;
    }

    public void setVoteAverage(Float voteAverage) {
        this.voteAverage = voteAverage;
    }

    public Integer getVoteCount() {
        return voteCount;
    }

    public void setVoteCount(Integer voteCount) {
        this.voteCount = voteCount;
    }

    public LocalDate getLastTMDBUpdate() {
        return lastTMDBUpdate;
    }

    public void setLastTMDBUpdate(LocalDate lastTMDBUpdate) {
        this.lastTMDBUpdate = lastTMDBUpdate;
    }

    public MovieDTO getPosterMovie() {
        return posterMovie;
    }

    public void setPosterMovie(MovieDTO posterMovie) {
        this.posterMovie = posterMovie;
    }

    public MovieDTO getBackdropMovie() {
        return backdropMovie;
    }

    public void setBackdropMovie(MovieDTO backdropMovie) {
        this.backdropMovie = backdropMovie;
    }

    public PersonDTO getPerson() {
        return person;
    }

    public void setPerson(PersonDTO person) {
        this.person = person;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ImageDTO)) {
            return false;
        }

        ImageDTO imageDTO = (ImageDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, imageDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ImageDTO{" +
            "id='" + getId() + "'" +
            ", tmdbId='" + getTmdbId() + "'" +
            ", locale='" + getLocale() + "'" +
            ", voteAverage=" + getVoteAverage() +
            ", voteCount=" + getVoteCount() +
            ", lastTMDBUpdate='" + getLastTMDBUpdate() + "'" +
            ", posterMovie='" + getPosterMovie() + "'" +
            ", backdropMovie='" + getBackdropMovie() + "'" +
            ", person='" + getPerson() + "'" +
            "}";
    }
}
