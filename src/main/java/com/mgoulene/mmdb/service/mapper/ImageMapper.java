package com.mgoulene.mmdb.service.mapper;

import com.mgoulene.mmdb.domain.*;
import com.mgoulene.mmdb.service.dto.ImageDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Image} and its DTO {@link ImageDTO}.
 */
@Mapper(componentModel = "spring", uses = { MovieMapper.class, PersonMapper.class })
public interface ImageMapper extends EntityMapper<ImageDTO, Image> {
    @Mapping(target = "posterMovie", source = "posterMovie", qualifiedByName = "title")
    @Mapping(target = "backdropMovie", source = "backdropMovie", qualifiedByName = "title")
    @Mapping(target = "person", source = "person", qualifiedByName = "name")
    ImageDTO toDto(Image s);

    @Named("id")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    ImageDTO toDtoId(Image image);
}
