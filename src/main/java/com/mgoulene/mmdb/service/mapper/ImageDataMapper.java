package com.mgoulene.mmdb.service.mapper;

import com.mgoulene.mmdb.domain.*;
import com.mgoulene.mmdb.service.dto.ImageDataDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link ImageData} and its DTO {@link ImageDataDTO}.
 */
@Mapper(componentModel = "spring", uses = { ImageMapper.class })
public interface ImageDataMapper extends EntityMapper<ImageDataDTO, ImageData> {
    @Mapping(target = "image", source = "image", qualifiedByName = "id")
    ImageDataDTO toDto(ImageData s);
}
