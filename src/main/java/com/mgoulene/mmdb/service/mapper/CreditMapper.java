package com.mgoulene.mmdb.service.mapper;

import com.mgoulene.mmdb.domain.*;
import com.mgoulene.mmdb.service.dto.CreditDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Credit} and its DTO {@link CreditDTO}.
 */
@Mapper(componentModel = "spring", uses = { PersonMapper.class, MovieMapper.class })
public interface CreditMapper extends EntityMapper<CreditDTO, Credit> {
    @Mapping(target = "person", source = "person", qualifiedByName = "name")
    @Mapping(target = "movie", source = "movie", qualifiedByName = "title")
    CreditDTO toDto(Credit s);
}
