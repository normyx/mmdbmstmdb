package com.mgoulene.mmdb.service;

import com.mgoulene.mmdb.domain.ImageData;
import com.mgoulene.mmdb.repository.ImageDataRepository;
import com.mgoulene.mmdb.service.dto.ImageDataDTO;
import com.mgoulene.mmdb.service.mapper.ImageDataMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * Service Implementation for managing {@link ImageData}.
 */
@Service
public class ImageDataService {

    private final Logger log = LoggerFactory.getLogger(ImageDataService.class);

    private final ImageDataRepository imageDataRepository;

    private final ImageDataMapper imageDataMapper;

    public ImageDataService(ImageDataRepository imageDataRepository, ImageDataMapper imageDataMapper) {
        this.imageDataRepository = imageDataRepository;
        this.imageDataMapper = imageDataMapper;
    }

    /**
     * Save a imageData.
     *
     * @param imageDataDTO the entity to save.
     * @return the persisted entity.
     */
    public ImageDataDTO save(ImageDataDTO imageDataDTO) {
        log.debug("Request to save ImageData : {}", imageDataDTO);
        ImageData imageData = imageDataMapper.toEntity(imageDataDTO);
        imageData = imageDataRepository.save(imageData);
        return imageDataMapper.toDto(imageData);
    }

    /**
     * Partially update a imageData.
     *
     * @param imageDataDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<ImageDataDTO> partialUpdate(ImageDataDTO imageDataDTO) {
        log.debug("Request to partially update ImageData : {}", imageDataDTO);

        return imageDataRepository
            .findById(imageDataDTO.getId())
            .map(
                existingImageData -> {
                    imageDataMapper.partialUpdate(existingImageData, imageDataDTO);
                    return existingImageData;
                }
            )
            .map(imageDataRepository::save)
            .map(imageDataMapper::toDto);
    }

    /**
     * Get all the imageData.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    public Page<ImageDataDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ImageData");
        return imageDataRepository.findAll(pageable).map(imageDataMapper::toDto);
    }

    /**
     * Get one imageData by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    public Optional<ImageDataDTO> findOne(String id) {
        log.debug("Request to get ImageData : {}", id);
        return imageDataRepository.findById(id).map(imageDataMapper::toDto);
    }

    /**
     * Delete the imageData by id.
     *
     * @param id the id of the entity.
     */
    public void delete(String id) {
        log.debug("Request to delete ImageData : {}", id);
        imageDataRepository.deleteById(id);
    }
}
