package com.mgoulene.mmdb.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import javax.validation.constraints.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * The Person entity.\n@author A true hipster
 */
@Document(collection = "person")
public class Person implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Field("birthday")
    private LocalDate birthday;

    @Field("deathday")
    private LocalDate deathday;

    @Size(max = 200)
    @Field("name")
    private String name;

    @Size(max = 200)
    @Field("aka")
    private String aka;

    @Field("gender")
    private Integer gender;

    @Size(max = 4000)
    @Field("biography")
    private String biography;

    @Size(max = 200)
    @Field("place_of_birth")
    private String placeOfBirth;

    @Size(max = 200)
    @Field("homepage")
    private String homepage;

    @NotNull
    @Field("last_tmdb_update")
    private LocalDate lastTMDBUpdate;

    @NotNull
    @Field("tmdb_id")
    private String tmdbId;

    @DBRef
    @Field("profile")
    @JsonIgnoreProperties(value = { "posterMovie", "backdropMovie", "person" }, allowSetters = true)
    private Set<Image> profiles = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Person id(String id) {
        this.id = id;
        return this;
    }

    public LocalDate getBirthday() {
        return this.birthday;
    }

    public Person birthday(LocalDate birthday) {
        this.birthday = birthday;
        return this;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public LocalDate getDeathday() {
        return this.deathday;
    }

    public Person deathday(LocalDate deathday) {
        this.deathday = deathday;
        return this;
    }

    public void setDeathday(LocalDate deathday) {
        this.deathday = deathday;
    }

    public String getName() {
        return this.name;
    }

    public Person name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAka() {
        return this.aka;
    }

    public Person aka(String aka) {
        this.aka = aka;
        return this;
    }

    public void setAka(String aka) {
        this.aka = aka;
    }

    public Integer getGender() {
        return this.gender;
    }

    public Person gender(Integer gender) {
        this.gender = gender;
        return this;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public String getBiography() {
        return this.biography;
    }

    public Person biography(String biography) {
        this.biography = biography;
        return this;
    }

    public void setBiography(String biography) {
        this.biography = biography;
    }

    public String getPlaceOfBirth() {
        return this.placeOfBirth;
    }

    public Person placeOfBirth(String placeOfBirth) {
        this.placeOfBirth = placeOfBirth;
        return this;
    }

    public void setPlaceOfBirth(String placeOfBirth) {
        this.placeOfBirth = placeOfBirth;
    }

    public String getHomepage() {
        return this.homepage;
    }

    public Person homepage(String homepage) {
        this.homepage = homepage;
        return this;
    }

    public void setHomepage(String homepage) {
        this.homepage = homepage;
    }

    public LocalDate getLastTMDBUpdate() {
        return this.lastTMDBUpdate;
    }

    public Person lastTMDBUpdate(LocalDate lastTMDBUpdate) {
        this.lastTMDBUpdate = lastTMDBUpdate;
        return this;
    }

    public void setLastTMDBUpdate(LocalDate lastTMDBUpdate) {
        this.lastTMDBUpdate = lastTMDBUpdate;
    }

    public String getTmdbId() {
        return this.tmdbId;
    }

    public Person tmdbId(String tmdbId) {
        this.tmdbId = tmdbId;
        return this;
    }

    public void setTmdbId(String tmdbId) {
        this.tmdbId = tmdbId;
    }

    public Set<Image> getProfiles() {
        return this.profiles;
    }

    public Person profiles(Set<Image> images) {
        this.setProfiles(images);
        return this;
    }

    public Person addProfile(Image image) {
        this.profiles.add(image);
        image.setPerson(this);
        return this;
    }

    public Person removeProfile(Image image) {
        this.profiles.remove(image);
        image.setPerson(null);
        return this;
    }

    public void setProfiles(Set<Image> images) {
        if (this.profiles != null) {
            this.profiles.forEach(i -> i.setPerson(null));
        }
        if (images != null) {
            images.forEach(i -> i.setPerson(this));
        }
        this.profiles = images;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Person)) {
            return false;
        }
        return id != null && id.equals(((Person) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Person{" +
            "id=" + getId() +
            ", birthday='" + getBirthday() + "'" +
            ", deathday='" + getDeathday() + "'" +
            ", name='" + getName() + "'" +
            ", aka='" + getAka() + "'" +
            ", gender=" + getGender() +
            ", biography='" + getBiography() + "'" +
            ", placeOfBirth='" + getPlaceOfBirth() + "'" +
            ", homepage='" + getHomepage() + "'" +
            ", lastTMDBUpdate='" + getLastTMDBUpdate() + "'" +
            ", tmdbId='" + getTmdbId() + "'" +
            "}";
    }
}
