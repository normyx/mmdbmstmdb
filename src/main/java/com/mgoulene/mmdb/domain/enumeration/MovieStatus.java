package com.mgoulene.mmdb.domain.enumeration;

/**
 * The MovieStatus enumeration.
 */
public enum MovieStatus {
    RUMORED,
    PLANNED,
    IN_PRODUCTION,
    POST_PRODUCTION,
    RELEASED,
    CANCELED,
}
