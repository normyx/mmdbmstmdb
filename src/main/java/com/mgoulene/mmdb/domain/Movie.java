package com.mgoulene.mmdb.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.mgoulene.mmdb.domain.enumeration.MovieStatus;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import javax.validation.constraints.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * The Movie entity.\n@author A true hipster
 */
@Document(collection = "movie")
public class Movie implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Size(max = 200)
    @Field("title")
    private String title;

    @Field("for_adult")
    private Boolean forAdult;

    @Field("homepage")
    private String homepage;

    @Field("original_language")
    private String originalLanguage;

    @Field("original_title")
    private String originalTitle;

    @Size(max = 4000)
    @Field("overview")
    private String overview;

    @Field("tagline")
    private String tagline;

    @Field("status")
    private MovieStatus status;

    @DecimalMin(value = "0")
    @DecimalMax(value = "10")
    @Field("vote_average")
    private Float voteAverage;

    @Field("vote_count")
    private Integer voteCount;

    @Field("release_date")
    private LocalDate releaseDate;

    @NotNull
    @Field("last_tmdb_update")
    private LocalDate lastTMDBUpdate;

    @NotNull
    @Field("tmdb_id")
    private String tmdbId;

    @Field("runtime")
    private Integer runtime;

    @DBRef
    @Field("credits")
    @JsonIgnoreProperties(value = { "person", "movie" }, allowSetters = true)
    private Set<Credit> credits = new HashSet<>();

    @DBRef
    @Field("poster")
    @JsonIgnoreProperties(value = { "posterMovie", "backdropMovie", "person" }, allowSetters = true)
    private Set<Image> posters = new HashSet<>();

    @DBRef
    @Field("backdrop")
    @JsonIgnoreProperties(value = { "posterMovie", "backdropMovie", "person" }, allowSetters = true)
    private Set<Image> backdrops = new HashSet<>();

    @DBRef
    @Field("genres")
    @JsonIgnoreProperties(value = { "movies" }, allowSetters = true)
    private Set<Genre> genres = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Movie id(String id) {
        this.id = id;
        return this;
    }

    public String getTitle() {
        return this.title;
    }

    public Movie title(String title) {
        this.title = title;
        return this;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean getForAdult() {
        return this.forAdult;
    }

    public Movie forAdult(Boolean forAdult) {
        this.forAdult = forAdult;
        return this;
    }

    public void setForAdult(Boolean forAdult) {
        this.forAdult = forAdult;
    }

    public String getHomepage() {
        return this.homepage;
    }

    public Movie homepage(String homepage) {
        this.homepage = homepage;
        return this;
    }

    public void setHomepage(String homepage) {
        this.homepage = homepage;
    }

    public String getOriginalLanguage() {
        return this.originalLanguage;
    }

    public Movie originalLanguage(String originalLanguage) {
        this.originalLanguage = originalLanguage;
        return this;
    }

    public void setOriginalLanguage(String originalLanguage) {
        this.originalLanguage = originalLanguage;
    }

    public String getOriginalTitle() {
        return this.originalTitle;
    }

    public Movie originalTitle(String originalTitle) {
        this.originalTitle = originalTitle;
        return this;
    }

    public void setOriginalTitle(String originalTitle) {
        this.originalTitle = originalTitle;
    }

    public String getOverview() {
        return this.overview;
    }

    public Movie overview(String overview) {
        this.overview = overview;
        return this;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getTagline() {
        return this.tagline;
    }

    public Movie tagline(String tagline) {
        this.tagline = tagline;
        return this;
    }

    public void setTagline(String tagline) {
        this.tagline = tagline;
    }

    public MovieStatus getStatus() {
        return this.status;
    }

    public Movie status(MovieStatus status) {
        this.status = status;
        return this;
    }

    public void setStatus(MovieStatus status) {
        this.status = status;
    }

    public Float getVoteAverage() {
        return this.voteAverage;
    }

    public Movie voteAverage(Float voteAverage) {
        this.voteAverage = voteAverage;
        return this;
    }

    public void setVoteAverage(Float voteAverage) {
        this.voteAverage = voteAverage;
    }

    public Integer getVoteCount() {
        return this.voteCount;
    }

    public Movie voteCount(Integer voteCount) {
        this.voteCount = voteCount;
        return this;
    }

    public void setVoteCount(Integer voteCount) {
        this.voteCount = voteCount;
    }

    public LocalDate getReleaseDate() {
        return this.releaseDate;
    }

    public Movie releaseDate(LocalDate releaseDate) {
        this.releaseDate = releaseDate;
        return this;
    }

    public void setReleaseDate(LocalDate releaseDate) {
        this.releaseDate = releaseDate;
    }

    public LocalDate getLastTMDBUpdate() {
        return this.lastTMDBUpdate;
    }

    public Movie lastTMDBUpdate(LocalDate lastTMDBUpdate) {
        this.lastTMDBUpdate = lastTMDBUpdate;
        return this;
    }

    public void setLastTMDBUpdate(LocalDate lastTMDBUpdate) {
        this.lastTMDBUpdate = lastTMDBUpdate;
    }

    public String getTmdbId() {
        return this.tmdbId;
    }

    public Movie tmdbId(String tmdbId) {
        this.tmdbId = tmdbId;
        return this;
    }

    public void setTmdbId(String tmdbId) {
        this.tmdbId = tmdbId;
    }

    public Integer getRuntime() {
        return this.runtime;
    }

    public Movie runtime(Integer runtime) {
        this.runtime = runtime;
        return this;
    }

    public void setRuntime(Integer runtime) {
        this.runtime = runtime;
    }

    public Set<Credit> getCredits() {
        return this.credits;
    }

    public Movie credits(Set<Credit> credits) {
        this.setCredits(credits);
        return this;
    }

    public Movie addCredits(Credit credit) {
        this.credits.add(credit);
        credit.setMovie(this);
        return this;
    }

    public Movie removeCredits(Credit credit) {
        this.credits.remove(credit);
        credit.setMovie(null);
        return this;
    }

    public void setCredits(Set<Credit> credits) {
        if (this.credits != null) {
            this.credits.forEach(i -> i.setMovie(null));
        }
        if (credits != null) {
            credits.forEach(i -> i.setMovie(this));
        }
        this.credits = credits;
    }

    public Set<Image> getPosters() {
        return this.posters;
    }

    public Movie posters(Set<Image> images) {
        this.setPosters(images);
        return this;
    }

    public Movie addPoster(Image image) {
        this.posters.add(image);
        image.setPosterMovie(this);
        return this;
    }

    public Movie removePoster(Image image) {
        this.posters.remove(image);
        image.setPosterMovie(null);
        return this;
    }

    public void setPosters(Set<Image> images) {
        if (this.posters != null) {
            this.posters.forEach(i -> i.setPosterMovie(null));
        }
        if (images != null) {
            images.forEach(i -> i.setPosterMovie(this));
        }
        this.posters = images;
    }

    public Set<Image> getBackdrops() {
        return this.backdrops;
    }

    public Movie backdrops(Set<Image> images) {
        this.setBackdrops(images);
        return this;
    }

    public Movie addBackdrop(Image image) {
        this.backdrops.add(image);
        image.setBackdropMovie(this);
        return this;
    }

    public Movie removeBackdrop(Image image) {
        this.backdrops.remove(image);
        image.setBackdropMovie(null);
        return this;
    }

    public void setBackdrops(Set<Image> images) {
        if (this.backdrops != null) {
            this.backdrops.forEach(i -> i.setBackdropMovie(null));
        }
        if (images != null) {
            images.forEach(i -> i.setBackdropMovie(this));
        }
        this.backdrops = images;
    }

    public Set<Genre> getGenres() {
        return this.genres;
    }

    public Movie genres(Set<Genre> genres) {
        this.setGenres(genres);
        return this;
    }

    public Movie addGenre(Genre genre) {
        this.genres.add(genre);
        genre.getMovies().add(this);
        return this;
    }

    public Movie removeGenre(Genre genre) {
        this.genres.remove(genre);
        genre.getMovies().remove(this);
        return this;
    }

    public void setGenres(Set<Genre> genres) {
        this.genres = genres;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Movie)) {
            return false;
        }
        return id != null && id.equals(((Movie) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Movie{" +
            "id=" + getId() +
            ", title='" + getTitle() + "'" +
            ", forAdult='" + getForAdult() + "'" +
            ", homepage='" + getHomepage() + "'" +
            ", originalLanguage='" + getOriginalLanguage() + "'" +
            ", originalTitle='" + getOriginalTitle() + "'" +
            ", overview='" + getOverview() + "'" +
            ", tagline='" + getTagline() + "'" +
            ", status='" + getStatus() + "'" +
            ", voteAverage=" + getVoteAverage() +
            ", voteCount=" + getVoteCount() +
            ", releaseDate='" + getReleaseDate() + "'" +
            ", lastTMDBUpdate='" + getLastTMDBUpdate() + "'" +
            ", tmdbId='" + getTmdbId() + "'" +
            ", runtime=" + getRuntime() +
            "}";
    }
}
