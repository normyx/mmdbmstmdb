package com.mgoulene.mmdb.repository;

import com.mgoulene.mmdb.domain.ImageData;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the ImageData entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ImageDataRepository extends MongoRepository<ImageData, String> {}
