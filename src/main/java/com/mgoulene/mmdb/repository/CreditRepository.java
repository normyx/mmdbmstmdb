package com.mgoulene.mmdb.repository;

import com.mgoulene.mmdb.domain.Credit;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the Credit entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CreditRepository extends MongoRepository<Credit, String> {}
