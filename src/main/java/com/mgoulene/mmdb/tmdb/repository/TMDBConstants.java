package com.mgoulene.mmdb.tmdb.repository;

public interface TMDBConstants {
    public static final String TMDB_IMAGE_URL = "https://image.tmdb.org";
    public static final String TMDB_API_URL = "https://api.themoviedb.org/3";
}
