package com.mgoulene.mmdb.tmdb.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Arrays;

public class TMDBPersonImageDTO {

    @JsonProperty
    private TMDBImageDTO[] profiles;

    private String id;

    public TMDBPersonImageDTO() {}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public TMDBImageDTO[] getProfiles() {
        return profiles;
    }

    public void setProfiles(TMDBImageDTO[] profiles) {
        this.profiles = profiles;
    }

    @Override
    public String toString() {
        return "TMDBPersonImageDTO [id=" + id + ", profiles=" + Arrays.toString(profiles) + "]";
    }
}
