package com.mgoulene.mmdb.tmdb.service.mapper;

import com.mgoulene.mmdb.domain.Movie;
import com.mgoulene.mmdb.service.dto.MovieDTO;
import com.mgoulene.mmdb.tmdb.domain.TMDBMovie;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

/**
 * Mapper for the entity {@link Movie} and its DTO {@link MovieDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface TMDBMovieMapper extends TMDBMapper<TMDBMovie, MovieDTO> {
    @Mapping(target = "genres", ignore = true)
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "status", ignore = true)
    @Mapping(target = "tmdbId", source = "id")
    //@Mapping(target = "posterTmdbId", source = "posterPath")
    MovieDTO toDTO(TMDBMovie tmdbMovieDTO);

    @Mapping(target = "genres", ignore = true)
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "status", ignore = true)
    @Mapping(target = "tmdbId", source = "id")
    //@Mapping(target = "posterTmdbId", source = "posterPath")
    void updateDTO(TMDBMovie tmdbMovieDTO, @MappingTarget MovieDTO movieDTO);
}
