package com.mgoulene.mmdb.tmdb.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.mgoulene.mmdb.tmdb.domain.TMDBMovie;

public class TMDBMovieSearchResponse {

    @JsonProperty
    private TMDBMovie[] results;

    public TMDBMovie[] getResults() {
        return results;
    }

    public void setResults(TMDBMovie[] results) {
        this.results = results;
    }

    public TMDBMovieSearchResponse() {}

    @Override
    public String toString() {
        String res = "Response [results=\n";
        for (int i = 0; i < results.length; i++) {
            res += results[i] + "\n";
        }
        res += "]";
        return res;
    }
}
