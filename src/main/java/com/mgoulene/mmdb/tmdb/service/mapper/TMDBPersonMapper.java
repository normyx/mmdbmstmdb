package com.mgoulene.mmdb.tmdb.service.mapper;

import com.mgoulene.mmdb.domain.Movie;
import com.mgoulene.mmdb.service.dto.MovieDTO;
import com.mgoulene.mmdb.service.dto.PersonDTO;
import com.mgoulene.mmdb.tmdb.service.dto.TMDBPersonDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

/**
 * Mapper for the entity {@link Movie} and its DTO {@link MovieDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface TMDBPersonMapper extends TMDBMapper<TMDBPersonDTO, PersonDTO> {
    PersonDTO toDTO(TMDBPersonDTO tmdbPersonDTO);

    @Mapping(target = "id", ignore = true)
    void updateDTO(TMDBPersonDTO tmdbPersonDTO, @MappingTarget PersonDTO personDTO);
}
