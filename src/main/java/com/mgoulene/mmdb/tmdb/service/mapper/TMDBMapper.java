package com.mgoulene.mmdb.tmdb.service.mapper;

import java.util.List;

/**
 * Contract for a generic dto to entity mapper.
 *
 * @param <D> - DTO type parameter.
 * @param <E> - Entity type parameter.
 */

public interface TMDBMapper<D, E> {
    E toDTO(D tmdbDTO);

    //void updateDTO(D tmdbDTO, E dto);

    List<E> toDTO(List<D> tmdbDTOList);
    //void updateDTO(List <D> tmdbDTOList, List<E> dtoList);
}
