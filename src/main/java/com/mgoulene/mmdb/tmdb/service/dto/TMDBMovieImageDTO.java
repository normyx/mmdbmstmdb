package com.mgoulene.mmdb.tmdb.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Arrays;

public class TMDBMovieImageDTO {

    @JsonProperty
    private TMDBImageDTO[] backdrops;

    @JsonProperty
    private TMDBImageDTO[] posters;

    private String id;

    public TMDBMovieImageDTO() {}

    public TMDBImageDTO[] getBackdrops() {
        return backdrops;
    }

    public void setBackdrops(TMDBImageDTO[] backdrops) {
        this.backdrops = backdrops;
    }

    public TMDBImageDTO[] getPosters() {
        return posters;
    }

    public void setPosters(TMDBImageDTO[] posters) {
        this.posters = posters;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "TMDBMovieImageDTO [backdrops=" + Arrays.toString(backdrops) + ", id=" + id + ", posters=" + Arrays.toString(posters) + "]";
    }
}
